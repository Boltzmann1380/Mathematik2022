# Mathematik Abitur Vorbereitung 2022
Dieses Repository soll Informationen zur Vorbereitung auf das mündliche Abitur bereitstellen.

## Themen des Kurses
Wir möchten folgende Themen nach- bzw. bearbeiten (12er und 13er vorhanden):
- Analysis (Stammfunktionen und Integrale)
- Stochastik (Jahrgang 12 und Jahrgang 13)
- Jahrgang 13 begleitend Geraden/Ebenen (analytische Geometrie)

## Notizen zu schon behandelten Themen
### 12er Niveau
- Wir haben eine Aufgabe besprochen, bei der aus einem Topf mit 5 Sicherungen, von denen 2 defekt sind, ausgerechnet werden sollte, wie groß die Wahrscheinlichkeite ist, mindestens eine Defekte bei zwei Herausnahmen einer Sicherung zu ziehen.

### 13er Niveau
- Um überhaupt zu verstehen, wie man eine Aufleitung (Stammfunktion) berechnen kann und wofür sie genutzt werden kann, wurden für Polynome (f(x) mit Termen, die x^3 und/oder x^2 und/oder mx+b Aufleitungen und Ableitungen davon bestimmt. Dafür haben wir Teile von Funktionsdiskussionen angerissen. Dabei haben wir die ([https://de.wikibooks.org/wiki/Mathe_f%C3%BCr_Nicht-Freaks:_Hauptsatz_der_Differential-_und_Integralrechnung](hier: erste Variante)) des Hauptsatzes der Differential- und Integralrechnung genutzt.

## Links
Das Landesinstitut für Lehrerbildung und Schulentwicklung stellt [Aufgaben für die Abiturvorbereitung bereit](https://li.hamburg.de/abiturpruefung/). Von diesen Aufgaben stelle ich hier den [hilfsmittelfreien](https://li.hamburg.de/contentblob/9307932/2373ade4d2e6fe66ea252e1cc3bea46e/data/d-2017-08-10-mathematik-abitur-pruefungsteil-a.pdf) und [den Teil, in dem Hilfsmittel benutzt werden dürfen,](https://li.hamburg.de/contentblob/9307936/7f53c32bf9cfc6b6e7d1200af232e1cf/data/d-2017-08-10-mathematik-abitur-pruefungsteil-b.pdf) bereit.

Diese Aufgaben sind nur für den schriftlichen Teil des Mathematik Abiturs. Man könnte zur Vorbereitung auf die mündlichen Abiturprüfungen diese Aufgaben bearbeiten, aber wie in einer mündlichen Prüfung besprechen.


## Kontakt
stefan.bollmann@rwth-aachen.de
